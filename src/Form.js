import React, { Component } from 'react';

class Form extends Component {
constructor(props) {
    super(props);
    this.state = {
      filmName: '',
      URL: '',
      comment: '',
    }
    this.onChange = this.onChange.bind(this);
    this.submitForm = this.submitForm.bind(this);
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  submitForm(e) {
    e.preventDefault();

  const config = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
        title: this.state.filmName,
        poster: this.state.URL,
        comment: this.state.comment

    }),
  };

  const url = "https://post-a-form.herokuapp.com/api/movies/"

  fetch(url, config)
    .then(res => res.json())
    .then(res => {
    if (res.error) {
      alert(res.error);
    } else {
      alert(`Saved ${res.title} movie!`)
    }
  })
  .catch(e => {
    console.error(e);
    alert('Movie not saved')
  });

}

 render () {
     return (
        <div className="FormFilm">
            <h1> Favorite film entry</h1>
            <form onSubmit={this.submitForm}>
            <fieldset>
            <legend>Information</legend>
            <div className="form-data">
            <label htmlFor="filmName">Film Name</label>
            <input
                type="text"
                id="filmName"
                name="filmName"
                onChange={this.onChange}
                value={this.state.filmName}
            />
        </div>

        <div className="form-data">
        <label htmlFor="URL">URL</label>
        <input
          type="text"
          id="URL"
          name="URL"
          onChange={this.onChange}
          value={this.state.URL}
        />
        </div>

            <div className="form-data">
            <label htmlFor="comment">Comment</label>
            <textarea
                type="text"
                id="comment"
                name="comment"
                onChange={this.onChange}
                value={this.state.comment}
            />
            </div>
            <hr />
            <div className="form-data">
            <input type="submit" value="Send" />
        </div>
        </fieldset>
    </form>
    </div>

    );
  }

}


export default Form;
